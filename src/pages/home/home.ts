import {Component, ViewChild, ElementRef, OnInit} from '@angular/core';
import {NavController, AlertController} from 'ionic-angular';
import {HttpClient} from '@angular/common/http'
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment
} from '@ionic-native/google-maps';
import {DatePicker} from '@ionic-native/date-picker';
import {ToastController} from 'ionic-angular';
import {CrimeProvider} from "../../providers/crime/crime";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  now = new Date()
  month = (this.now.getMonth() + 1) < 10 ? '0' + (this.now.getMonth() + 1) : (this.now.getMonth() + 1);
  dateYM = '2018-07';
  date = this.dateYM + '-01';
  markers = [];
  newLoc = false;
  crimeCategory = 'all-crime';
  latLng = {
    lat: 51.5085300,
    lng: -0.1257400
  }

  categories;
  crimesData;

  @ViewChild('map') mapElement: ElementRef;
  map: GoogleMap;

  constructor(public navCtrl: NavController,
              private http: HttpClient,
              public alertCtrl: AlertController,
              private datePicker: DatePicker,
              public toastCtrl: ToastController,
              private crimeProvider: CrimeProvider) {

  }

  ngOnInit() {
    this.crimeProvider.getCrimeCategory(this.dateYM).subscribe(data => {
      this.categories = data;
    });
    this.loadMap();
    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(event => {
      let json = JSON.parse(event);
      if (this.newLoc) {
        this.latLng.lat = json.lat;
        this.latLng.lng = json.lng;
        this.updateMarkers();
      }
    });
  }

  // всплывающие сообщения
  presentToast(mes) {
    const toast = this.toastCtrl.create({
      message: mes,
      duration: 3000
    });
    toast.present();
  }

  // загрузка карты
  loadMap() {

    // This code is necessary for browser
    // Environment.setEnv({
    //   'API_KEY_FOR_BROWSER_RELEASE': 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBdWJ_Vr-y8EO-FTpavmDFlDT52gDyA11E',
    //   'API_KEY_FOR_BROWSER_DEBUG': 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBdWJ_Vr-y8EO-FTpavmDFlDT52gDyA11E'
    // });

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: this.latLng,
        zoom: 15,
        tilt: 30
      }
    };

    let element = this.mapElement.nativeElement;
    this.map = GoogleMaps.create(element, mapOptions);
    this.addMainMarker();
  }

  // обновляем все маркеры
  updateMarkers() {

    this.map.clear();

    this.addMainMarker();

    this.crimeProvider.getCrimeMarkers(this.crimeCategory, this.dateYM, this.latLng)
      .subscribe(data => {
        this.crimesData = data;
      })
    for (let key in this.crimesData) {
      this.addMarker(this.crimesData[key]);
    }
    this.presentToast('Updating markers');

    this.newLoc = false;

  }

  // устанавливаем точку расчета
  addMainMarker() {
    var marker = this.map.addMarkerSync({
      title: 'search of crimes from that point in a radius of 1 mile',
      icon: 'red',
      position: this.latLng,
      map: this.map
    });
    this.markers.push(marker)
  }

  // отображение выбора даты
  showDatePicer() {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      (date: Date) => {
        let month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
        this.dateYM = date.getFullYear() + '-' + month;
        this.updateMarkers()
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  // метод для добавления маркера
  addMarker(data) {
    var latLng = {
      lat: data.location.latitude,
      lng: data.location.longitude
    };
    var marker = this.map.addMarkerSync({
      title: 'Outcome status: ' + data.outcome_status.category + '\nAdress: ' + data.location.street.name,
      icon: 'blue',
      position: latLng,
      map: this.map
    });
    this.markers.push(marker)
  }

  // отображаем выбор категории преступления
  showRadio() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Chose crime category');
    for (let key in this.categories) {
      alert.addInput({
        type: 'radio',
        label: this.categories[key]['name'],
        value: this.categories[key]['url'],
        checked: this.categories[key]['url'] == 'all-crime' ? true : false
      });
    }

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.crimeCategory = data;
        this.presentToast('Updated crime category')
        this.updateMarkers();
      }
    });
    alert.present();
  }

  // отображаем выбор локации
  showChoseLocation() {
    const confirm = this.alertCtrl.create({
      title: 'Want to specify new coordinates to search for crimes?',
      buttons: [
        {
          text: 'No'
        },
        {
          text: 'Yes',
          handler: () => {
            this.newLoc = true;
            this.presentToast('Select a point on the map to search for crimes')
          }
        }
      ]
    });
    confirm.present();
  }

}

