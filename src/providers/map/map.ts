import {Injectable} from '@angular/core';
import {GoogleMapOptions, GoogleMap, GoogleMaps, GoogleMapsEvent} from "@ionic-native/google-maps";

@Injectable()
export class MapProvider {

  constructor(public googleMaps: GoogleMaps) { }

}
