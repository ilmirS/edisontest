import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CrimeProvider {

  constructor(public http: HttpClient) {  }

  // получаем категории преступлений
  getCrimeCategory(dateYM) {
    return this.http.get('https://data.police.uk/api/crime-categories?date=' + dateYM);
  }

  getCrimeMarkers(crimeCategory, dateYM, latLng){
    let url = 'https://data.police.uk/api/crimes-street/';
    url += crimeCategory + '?';
    url += 'lat=' + latLng.lat + '&lng=' + latLng.lng;
    url += '&date=' + dateYM;

    return this.http.get(url)
  }
}



