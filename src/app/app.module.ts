import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { GoogleMaps } from '../../node_modules/@ionic-native/google-maps';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { DatePicker } from '@ionic-native/date-picker';
import { CrimeProvider } from '../providers/crime/crime';
import { MapProvider } from '../providers/map/map';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GoogleMaps,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpClient,
    DatePicker,
    CrimeProvider,
    MapProvider
  ]
})
export class AppModule {}
